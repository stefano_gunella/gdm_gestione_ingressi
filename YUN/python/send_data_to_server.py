# =====================================================
# deve collegarsi al server e mandare i dati aggiornati
# =====================================================
1. collegarsi al DB
2. inviare i dati al server
3. leggere la risposta
4. aggiornare il DB coi dati inviati

#!/usr/bin/python

import sqlite3 as lite
import sys

con = lite.connect('/mnt/sda1/arduino/www/test/ec.db')

with con:
    cur = con.cursor()
    cur.execute("SELECT * FROM Navn")
    rows = cur.fetchall()
    for row in rows:
        print row
