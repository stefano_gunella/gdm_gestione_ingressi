-- ===========
-- RFID_IN_OUT
-- ===========
DROP TABLE IF EXISTS RFID_IN_OUT;
CREATE TABLE RFID_IN_OUT(
	id 				INTEGER PRIMARY KEY, 	-- PK della tabella
	serial_card 	TEXT,					-- numero seriale della carta RFID
	in_out			TEXT, 					-- IN | OUT
	data			DATETIME,				-- giorno di ingresso con HH:MM
	device_id		TEXT,					-- identificativo della sorgente YUN,WF1,WF2,ecc...
	f_sended		INTEGER DEFAULT 0		-- 1 = spedito, 0 = spedire
);

-- =============
-- data test
-- =============
-- INSERT INTO RFID_IN_OUT (serial_card, in_out, data, device_id, f_sended) VALUES('XYZ12345','IN',CURRENT_TIMESTAMP, "YUN", 0);
