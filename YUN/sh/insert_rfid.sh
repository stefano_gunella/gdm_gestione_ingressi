#!/bin/sh
serial_card=$1
in_out=$2
device_id=$3
today=$(date)
sqlite3 /mnt/sda1/RFID_IN_OUT.db <<EOF
	INSERT INTO RFID_IN_OUT (serial_card, in_out, data, device_id, f_sended) VALUES("$serial_card", "$in_out", CURRENT_TIMESTAMP, "$device_id", 0);
EOF
echo "$today: $serial_card $in_out">>/mnt/sda1/db.log
echo "OK"
exit 0
