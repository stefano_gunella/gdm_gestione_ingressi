#!/bin/sh
n_reboot=0
if [[ -e "/mnt/sda1/restart_arduino.log" ]]; then
        n_reboot=$(cat /mnt/sda1/start_arduino.log|wc -l)
        n_reboot=$(($nreboot/2))
fi
echo "reboot n:$n_reboot"
today=$(date)
echo "today is $today and this is the $n_reboot" >> /mnt/sda1/start_arduino.log

node /mnt/sda1/server.js &
echo "started node server..." >> /mnt/sda1/start_arduino.log
