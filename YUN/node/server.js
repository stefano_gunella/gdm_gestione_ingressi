require('dotenv').config(); 					// npm install dotenv --> $Env:SQLITE_DB = "G:\projects\GDM_gestione_ingressi\YUN\sqllite3\rfid_timbratura\RFID_IN_OUT.db"
var http = require('http');
var url = require('url');
var sqlite3 = require('sqlite3').verbose(); 	// npm install sqlite3
var dblite  = require('dblite');				// npm install dblite


var server = http.createServer(function(req, res) {
	res.writeHead(200);
	res.end('Ciao a tutti, sono un web server!');
	console.log(req.method+"|"+req.url);
	// ==========================
	// READ parameters from ESP32
	// ==========================
	if (req.method === "GET") {
		var url_parts = url.parse(req.url,true);
		var serial_card = url_parts.query.serial_card;
		var in_out		= url_parts.query.in_out
		var device_id	= url_parts.query.device_id
		// use_sqlite3(serial_card,in_out,device_id);
		use_dblite(serial_card,in_out,device_id);
	}
	// res.end('INSERT');
});
server.listen(8080);

function use_dblite(serial_card, in_out, device_id){
	if(!(serial_card === undefined || in_out === undefined || device_id === undefined)){
		console.log(serial_card);
		console.log(in_out);
		console.log(device_id);
		// =============
		// open database
		// =============
		// W10 		$Env:SQLITE_DB = "G:\projects\GDM_gestione_ingressi\YUN\sqllite3\rfid_timbratura"
		// LINUX	export SQLITE_DB = "/mnt/sda/RFID_IN_OUT.db"
		console.log("open DB on "+process.env.SQLITE_DB);
		var db = dblite(process.env.SQLITE_DB);

		// ================
		// INSERT STATEMENT
		// ================
		let values = [serial_card, in_out, device_id];
		let sql = "INSERT INTO RFID_IN_OUT (serial_card, in_out, data, device_id, f_sended) VALUES (?, ?, datetime(CURRENT_TIMESTAMP, 'localtime'), ?, 0);";
		db.query(sql, values);
		// =============================
		// close the database connection
		// =============================
		db.close();
	}
}

function use_sqlite3(serial_card, in_out, device_id){
	console.log(serial_card);
	console.log(in_out);
	console.log(device_id);
	if(!(serial_card === undefined || in_out === undefined || device_id === undefined)){
		// =============
		// open database
		// =============
		// W10 		$Env:SQLITE_DB = "G:\projects\GDM_gestione_ingressi\YUN\sqllite3\rfid_timbratura"
		// LINUX	export $SQLITE_DB = "/mnt/sda/RFID_IN_OUT.db"
		console.log("open DB on "+process.env.SQLITE_DB);
		let db = new sqlite3.Database(process.env.SQLITE_DB, sqlite3.OPEN_READWRITE, (err) => {
			if (err) {
				return console.error(err.message);
			}
			console.log('Connected to the SQlite database.');
		});

		// ================
		// INSERT STATEMENT
		// ================
		let values = [serial_card, in_out, device_id];
		let sql = "INSERT INTO RFID_IN_OUT (serial_card, in_out, data, device_id, f_sended) VALUES (?, ?, datetime(CURRENT_TIMESTAMP, 'localtime'), ?, 0);";
		db.run(sql, values, function(err) {
			if (err) {
				return console.error(err.message);
			}
			console.log(`Rows inserted ${this.changes}`);
		});
		// =============================
		// close the database connection
		// =============================
		db.close((err) => {
			if (err) {
				return console.error(err.message);
			}
			console.log('Close the database connection.');
		});
	}
}
