/*
 * Initial Author: ryand1011 (https://github.com/ryand1011)
 *
 * Reads data written by a program such as "rfid_write_personal_data.ino"
 *
 * See: https://github.com/miguelbalboa/rfid/tree/master/examples/rfid_write_personal_data
 *
 * Uses MIFARE RFID card using RFID-RC522 reader
 * Uses MFRC522 - Library
 * -----------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
 *             Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin
 * -----------------------------------------------------------------------------------------
 * RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
 * SPI SS      SDA(SS)      10            53        D10        10               10
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
*/

#include <SPI.h>
#include <MFRC522.h>
#include <Bridge.h>
#include <Process.h>
#include "buzzer.h"

#define RST_PIN         9             // Configurable, see typical pin layout above
#define SS_PIN_IN       10            // Configurable, see typical pin layout above
#define SS_PIN_OUT      11            // Configurable, see typical pin layout above
#define LED_PIN_OK      2             // Led verde
#define LED_PIN_KO      3             // Led rosso
#define SOURCE_IN       String("YUN") // Sorgente Input

MFRC522 mfrc522_IN(SS_PIN_IN, RST_PIN);    // Create MFRC522 instance for IN
MFRC522 mfrc522_OUT(SS_PIN_OUT, RST_PIN);  // Create MFRC522 instance for OUT
MFRC522::StatusCode status;

// ==============================================================================
// Known keys, see: https://code.google.com/p/mfcuk/wiki/MifareClassicDefaultKeys
// ==============================================================================
#define NR_KNOWN_KEYS   8
byte knownKeys[NR_KNOWN_KEYS][MFRC522::MF_KEY_SIZE] =  {
    {0xff, 0xff, 0xff, 0xff, 0xff, 0xff}, // FF FF FF FF FF FF = factory default
    {0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5}, // A0 A1 A2 A3 A4 A5
    {0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5}, // B0 B1 B2 B3 B4 B5
    {0x4d, 0x3a, 0x99, 0xc3, 0x51, 0xdd}, // 4D 3A 99 C3 51 DD
    {0x1a, 0x98, 0x2c, 0x7e, 0x45, 0x9a}, // 1A 98 2C 7E 45 9A
    {0xd3, 0xf7, 0xd3, 0xf7, 0xd3, 0xf7}, // D3 F7 D3 F7 D3 F7
    {0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff}, // AA BB CC DD EE FF
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00}  // 00 00 00 00 00 00
};
//*****************************************************************************************//
void setup() {
  SPI.begin();                                                  // Init SPI bus
  Bridge.begin();
  Serial.begin(9600);                                           // Initialize the Serial
  mfrc522_IN.PCD_Init();                                        // Init MFRC522 card IN
  mfrc522_OUT.PCD_Init();                                       // Init MFRC522 card OUT
  pinMode(LED_PIN_OK, OUTPUT);
  pinMode(LED_PIN_KO, OUTPUT);
  // Serial.println(F("Read personal data on a MIFARE PICC:")); //shows in serial that it is ready to read
  // while (!Serial);
  Serial.println("START SERIAL USB");
}

//*****************************************************************************************//
void loop() {
  read_card(&mfrc522_IN,String("IN"));
  read_card(&mfrc522_OUT,String("OUT"));
  digitalWrite(LED_PIN_OK, LOW);          // OK
  digitalWrite(LED_PIN_KO, LOW);          // KO
}

/*
 * Helper routine to dump a byte array as hex values to Serial.
 */
void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], HEX);
    }
}

void reactive_mfrc522(MFRC522 *mfrc522){
    // http://arduino.stackexchange.com/a/14316
    mfrc522->PICC_IsNewCardPresent();
    mfrc522->PICC_ReadCardSerial();
}

void read_card(MFRC522 *mfrc522, String IN_OUT){
  // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
  if ( ! mfrc522->PICC_IsNewCardPresent()) {
    return;
  }

  // Select one of the cards
  if ( ! mfrc522->PICC_ReadCardSerial()) {
    return;
  }
  Serial.println("**Card Detected:**"+IN_OUT);
  Serial.print(F("PICC type: "));
  MFRC522::PICC_Type piccType = mfrc522->PICC_GetType(mfrc522->uid.sak);
  Serial.println(mfrc522->PICC_GetTypeName(piccType));
  String _serial_card = getSerialCard(&(mfrc522->uid));
  Serial.println("UID: "+_serial_card);
  if (send_data_to_linux(&(mfrc522->uid),IN_OUT)){
  }
  mfrc522->PICC_HaltA();
  mfrc522->PCD_StopCrypto1();
}

// ============================
// Legge il seriale della carta
// ============================
String getSerialCard(MFRC522::Uid* uid){
    String result;
    result  = String(uid->uidByte[0],HEX);
    result += String(uid->uidByte[1],HEX);
    result += String(uid->uidByte[2],HEX);
    result += String(uid->uidByte[3],HEX);
    return result;
}

boolean send_data_to_linux(MFRC522::Uid * uid, String in_out) {
  Process p;
  boolean result = false;
  String _serial_card = getSerialCard(uid);
  p.runShellCommand("/mnt/sda1/insert_rfid.sh "+_serial_card+" "+in_out+" "+SOURCE_IN);
  // p.runShellCommandAsynchronously("/mnt/sda1/insert_rfid.sh "+_serial_card+" "+in_out+" "+SOURCE_IN);
  // ==================================
  /*
  p.begin("python");
  p.addParameter("/mnt/sda1/insert_rfid.py");
  p.run(); // blocking call to run python; ATMega execution halts until complete
  */
  // ==================================

  Serial.println("running["+String(p.running())+"]");
  // do nothing until the process finishes, so you get the whole output:
  while (p.running());
  Serial.println("available["+String(p.available())+"]");
  // Read command output. runShellCommand() should have passed "Signal: xx&":
  String p_result = "KO";
  while (p.available()>0){
    p_result = p.readString();     // look for an integer
    Serial.print(">>>"+p_result);
  }
  if(p_result.compareTo("OK")){
    digitalWrite(LED_PIN_OK, HIGH);       // OK
    play_OK();
  }
  else{
    digitalWrite(LED_PIN_KO, HIGH);       // KO
    play_KO();
  }  
  Serial.flush();
  Serial.println("FINITO:"+_serial_card);
  Serial.println("=====================");
  return true;
}
