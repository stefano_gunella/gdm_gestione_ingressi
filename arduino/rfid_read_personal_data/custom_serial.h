void print(String msg){
  if (Serial){
    Serial.print(msg);  
  }
  if (Console){
    Console.print(msg);  
  }
}

void print_HEX(byte b){
  if (Serial){
    Serial.print(b,HEX);  
  }
  if (Console){
    Console.print(b,HEX);  
  }
}

void println(String msg){
  if (Serial){
    Serial.println(msg);  
  }
  if (Console){
    Console.println(msg);  
  }
}

void println_HEX(byte b){
  if (Serial){
    Serial.println(b,HEX);  
  }
  if (Console){
    Console.println(b,HEX);  
  }
}

void flush(){
  if (Serial){
    Serial.flush();  
  }
  if (Console){
    Console.flush();  
  }

}
