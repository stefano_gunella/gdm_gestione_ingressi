/*
 * Initial Author: ryand1011 (https://github.com/ryand1011)
 *
 * Reads data written by a program such as "rfid_write_personal_data.ino"
 *
 * See: https://github.com/miguelbalboa/rfid/tree/master/examples/rfid_write_personal_data
 *
 * Uses MIFARE RFID card using RFID-RC522 reader
 * Uses MFRC522 - Library
 * -----------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
 *             Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin
 * -----------------------------------------------------------------------------------------
 * RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
 * SPI SS      SDA(SS)      10            53        D10        10               10
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
*/

#include <SPI.h>
#include <MFRC522.h>
#include <Bridge.h>
#include <Process.h>
#include "custom_serial.h"
#include "PIN_CONFIG.h"
#include "buzzer.h"
#include "synch_date.h"

#define DEVICE_SOURCE_INPUT       String("YUN") // Sorgente Input YUN/ESP32

MFRC522 mfrc522_IN(SS_PIN_IN, RST_PIN_IN);    // Create MFRC522 instance for IN
MFRC522 mfrc522_OUT(SS_PIN_OUT, RST_PIN_OUT); // Create MFRC522 instance for OUT
MFRC522::StatusCode status;
bool IS_DATE_TIME_SYNCH = false;              // Controlla che la data sia aggiornata
// byte bufferATQA[20];                       // The buffer to store the ATQA (Answer to request) in
// byte bufferSize = 2;                       // Buffer size, at least two bytes. Also number of bytes returned if STATUS_OK.

// ===========================
// Inizializza il dispositivo:
// ===========================
void setup() {
    SPI.begin();                      // Init SPI bus
    Bridge.begin();                   // Init Bridge
    Console.begin();                  // Init Console
    Serial.begin(9600);               // Init the Serial
    pinMode(LED_PIN_OK, OUTPUT);
    pinMode(LED_PIN_KO, OUTPUT);
    //mfrc522_IN.PCD_Reset();
    //mfrc522_IN.PCD_AntennaOn();
    mfrc522_IN.PCD_Init();            // Init MFRC522 card IN
    delay(100);
    mfrc522_IN.PCD_DumpVersionToSerial();  // Show details of PCD - MFRC522 Card Reader details
    blink_LED(false,3);
    println("**mfrc522_IN:**");
    
    //mfrc522_OUT.PCD_Reset();
    //mfrc522_OUT.PCD_AntennaOn();
    mfrc522_OUT.PCD_Init();           // Init MFRC522 card OUT
    mfrc522_OUT.PCD_DumpVersionToSerial();  // Show details of PCD - MFRC522 Card Reader details
    println("**mfrc522_OUT:**");
    delay(100);
    play_READY();
    while (!IS_DATE_TIME_SYNCH){
      IS_DATE_TIME_SYNCH = check_date_time();
    }
    println("- SETUP END -"); // Qui aggiungere la parte che visualizza il msg sullo schermo
}

//*****************************************************************************************//
void loop() {
  read_card(&mfrc522_IN,String("IN"));
  read_card(&mfrc522_OUT,String("OUT"));
  digitalWrite(LED_PIN_OK, LOW);          // OK
  digitalWrite(LED_PIN_KO, LOW);          // KO
}

/*
 * Helper routine to dump a byte array as hex values to Serial.
 */
void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        print(buffer[i] < 0x10 ? " 0" : " ");
        print_HEX(buffer[i]);
    }
}

void reactive_mfrc522(MFRC522 *mfrc522){
    // http://arduino.stackexchange.com/a/14316
    mfrc522->PICC_IsNewCardPresent();
    mfrc522->PICC_ReadCardSerial();
}

void read_card(MFRC522 *mfrc522, String IN_OUT){
  // byte STATUS = mfrc522->PICC_WakeupA(bufferATQA,&bufferSize);
  // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
  if ( ! mfrc522->PICC_IsNewCardPresent()) {
    return;
  }
  println("**Card present:**"+IN_OUT);
  // Select one of the cards
  if ( ! mfrc522->PICC_ReadCardSerial()) {
    return;
  }
  println("**Card Serial Detected:**"+IN_OUT);
  print(F("PICC type: "));
  MFRC522::PICC_Type piccType = mfrc522->PICC_GetType(mfrc522->uid.sak);
  println(mfrc522->PICC_GetTypeName(piccType));
  String _serial_card = getSerialCard(&(mfrc522->uid));
  println("UID: "+_serial_card);
  if (send_data_to_linux(&(mfrc522->uid),IN_OUT)){
  }
  mfrc522->PICC_HaltA();
  mfrc522->PCD_StopCrypto1();
}

// ============================
// Legge il seriale della carta
// ============================
String getSerialCard(MFRC522::Uid* uid){
    String result;
    result  = String(uid->uidByte[0],HEX);
    result += String(uid->uidByte[1],HEX);
    result += String(uid->uidByte[2],HEX);
    result += String(uid->uidByte[3],HEX);
    return result;
}

boolean send_data_to_linux(MFRC522::Uid * uid, String in_out) {
  Process p;
  boolean result = false;
  String _serial_card = getSerialCard(uid);
  p.runShellCommand("/mnt/sda1/insert_rfid.sh "+_serial_card+" "+in_out+" "+DEVICE_SOURCE_INPUT);
  // p.runShellCommandAsynchronously("/mnt/sda1/insert_rfid.sh "+_serial_card+" "+in_out+" "+DEVICE_SOURCE_INPUT);
  // ==================================
  /*
  p.begin("python");
  p.addParameter("/mnt/sda1/insert_rfid.py");
  p.run(); // blocking call to run python; ATMega execution halts until complete
  */
  // ==================================

  println("running["+String(p.running())+"]");
  // do nothing until the process finishes, so you get the whole output:
  while (p.running());
  println("available["+String(p.available())+"]");
  // Read command output. runShellCommand() should have passed "Signal: xx&":
  String p_result = "KO";
  while (p.available()>0){
    p_result = p.readString();     // look for an integer
    print(">>>"+p_result);
  }
  if(p_result.compareTo("OK")){
    digitalWrite(LED_PIN_OK, HIGH);       // OK
    play_OK();
  }
  else{
    digitalWrite(LED_PIN_KO, HIGH);       // KO
    play_KO();
  }  
  flush();
  println("FINITO:"+_serial_card);
  println("=====================");
  return true;
}
