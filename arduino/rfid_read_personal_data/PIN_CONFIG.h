#define RST_PIN_IN      9             // Configurable, see typical pin layout above
#define RST_PIN_OUT     9             // Configurable, see typical pin layout above
#define SS_PIN_IN       10            // Configurable, see typical pin layout above
#define SS_PIN_OUT      11            // Configurable, see typical pin layout above
#define LED_PIN_OK      2             // Led verde
#define LED_PIN_KO      3             // Led rosso
