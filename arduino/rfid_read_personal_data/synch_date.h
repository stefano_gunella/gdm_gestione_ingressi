/***************************************************
 * Public - Verifica la sincronizzazione con la DATA
 * Una volta verificato é pronto per inviare i dati
 ***************************************************/
void blink_LED(bool f_alternate, int n_blink){
  for (int i=0;i<n_blink;i++){
    if(!f_alternate || (f_alternate && (i%2)==0)){
      digitalWrite(LED_PIN_OK, HIGH);       // OK
    }
    if(!f_alternate || (f_alternate && (i%2)==1)){
      digitalWrite(LED_PIN_KO, HIGH);       // KO
    }
    delay(200);
    digitalWrite(LED_PIN_OK, LOW);       // OK
    digitalWrite(LED_PIN_KO, LOW);       // KO
  }
}
 
bool check_date_time(){
  Process p;
  boolean result = false;
  // p.runShellCommand("/mnt/sda1/insert_rfid.sh "+_serial_card+" "+in_out+" "+SOURCE_IN);
  // p.runShellCommandAsynchronously("/mnt/sda1/insert_rfid.sh "+_serial_card+" "+in_out+" "+SOURCE_IN);
  // ==================================
  p.begin("python");
  p.addParameter("/mnt/sda1/check_date.py");
  p.run(); // blocking call to run python; ATMega execution halts until complete
  // ==================================
  println("SYNCH DATE running ["+String(p.running())+"]");
  // do nothing until the process finishes, so you get the whole output:
  while (p.running());
  // Read command output.runShellCommand() should have passed "Signal: xx&":
  String s_result;
  long l_result = 20211010; 
  while (p.available()>0){
    s_result = p.readString();     
    l_result = atol(s_result.c_str());  // look for an integer
    print("TODAY:"+s_result);
  }
  if(l_result > 20211010){
    print("DATE OK:"+s_result);
    blink_LED(true,5);
    flush(); 
    return true;
  }
  blink_LED(false,5); 
  flush();
  return false;
}
