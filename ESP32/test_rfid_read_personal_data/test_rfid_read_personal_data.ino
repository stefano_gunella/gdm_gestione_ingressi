/*
 * Initial Author: ryand1011 (https://github.com/ryand1011)
 *
 * Reads data written by a program such as "rfid_write_personal_data.ino"
 *
 * See: https://github.com/miguelbalboa/rfid/tree/master/examples/rfid_write_personal_data
 *
 * Uses MIFARE RFID card using RFID-RC522 reader
 * Uses MFRC522 - Library
 * -----------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
 *             Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin
 * -----------------------------------------------------------------------------------------
 * RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
 * SPI SS      SDA(SS)      10            53        D10        10               10
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
*/

#include <SPI.h>
#include <MFRC522.h>
#include <Bridge.h>
#include <Process.h>

#define RST_PIN         9             // Configurable, see typical pin layout above
#define SS_PIN          10            // Configurable, see typical pin layout above
#define LED_PIN         2             // Led verde
#define SOURCE_IN       String("YUN") // Sorgente Input

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance

int cnt = 0;

//*****************************************************************************************//
void setup() {
  // Serial.begin(9600);                                        // Initialize serial communications with the PC
  SPI.begin();                                                  // Init SPI bus
  Bridge.begin();
  Serial.begin(9600);                                           // Initialize the Serial
  mfrc522.PCD_Init();                                           // Init MFRC522 card
  pinMode(2, OUTPUT);
  // Serial.println(F("Read personal data on a MIFARE PICC:")); //shows in serial that it is ready to read
  // while (!Serial);
  Serial.println("START SERIAL USB");
}

//*****************************************************************************************//
void loop() {
  // RESET CARD
  // mfrc522.PCD_Reset();
  
  // Prepare key - all keys are set to FF FF FF FF FF FFh at chip delivery from the factory.
  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;

  //some variables we need
  byte block;
  byte len;
  MFRC522::StatusCode status;

  //-------------------------------------------

  // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return;
  }

  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return;
  }

  Serial.println(F("**Card Detected:**"));

  //-------------------------------------------

  mfrc522.PICC_DumpDetailsToSerial(&(mfrc522.uid));   //dump some details about the card
  // mfrc522.PICC_DumpToSerial(&(mfrc522.uid));       //uncomment this to see all blocks in hex

  //-------------------------------------------

  byte buffer1[18];

  block = 4;
  len = 18;

  //------------------------------------------- GET FIRST NAME
  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 4, &key, &(mfrc522.uid)); //line 834 of MFRC522.cpp file
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Authentication failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }
  Serial.print(F("First Name: "));
  status = mfrc522.MIFARE_Read(block, buffer1, &len);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Reading failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  //PRINT FIRST NAME
  for (uint8_t i = 0; i < 16; i++){
    if (buffer1[i] != 32){
      Serial.write(buffer1[i]);
    }
  }
  Serial.println("");
  //---------------------------------------- GET LAST NAME

  byte buffer2[18];
  block = 1;

  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 1, &key, &(mfrc522.uid)); //line 834
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Authentication failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }
  Serial.print(F("Last Name:"));
  status = mfrc522.MIFARE_Read(block, buffer2, &len);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Reading failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  //PRINT LAST NAME
  for (uint8_t i = 0; i < 16; i++) {
    Serial.write(buffer2[i] );
  }
  
  //----------------------------------------

  Serial.println(F("\n**End Reading**\n"));
  if (send_data_to_linux(&(mfrc522.uid),"IN")){
    digitalWrite(2, HIGH);          // OK
  }

  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
  delay(300);
  digitalWrite(2, LOW);          // OK
}

// ============================
// Legge il seriale della carta
// ============================
String getSerialCard(MFRC522::Uid* uid){
    String result;
    result  = String(uid->uidByte[0],HEX);
    result += String(uid->uidByte[1],HEX);
    result += String(uid->uidByte[2],HEX);
    result += String(uid->uidByte[3],HEX);
    mfrc522.PICC_HaltA(); // Stop reading
    return String(result);
}

boolean send_data_to_linux(MFRC522::Uid * uid, char* in_out) {
  Process p;
  boolean result = false;
  String _serial_card = getSerialCard(uid);
  p.runShellCommand("/mnt/sda1/insert_rfid.sh "+_serial_card+" "+String(in_out)+" "+SOURCE_IN);
  // p.runShellCommand("echo 'SUKA' >> /mnt/sda1/SUKA.log"); -- WORKING OK
  // ==================================
  /*
  p.begin("python");
  p.addParameter("/mnt/sda1/insert_rfid.py");
  p.run(); // blocking call to run python; ATMega execution halts until complete
  */
  // ==================================

  Serial.print("running["+String(p.running())+"]");
  // do nothing until the process finishes, so you get the whole output:
  while (p.running());
  Serial.println("available["+String(p.available())+"]");
  // Read command output. runShellCommand() should have passed "Signal: xx&":
  while (p.available()) {
    Serial.println("COMMAND:");
    String result = p.readString();     // look for an integer
    Serial.println(result);             // print the number as well
  }
  Serial.flush();
  Serial.print("FINITO:"+_serial_card);
  cnt++;
  return true;
}
